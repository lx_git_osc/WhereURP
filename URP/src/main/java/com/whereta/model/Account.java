package com.whereta.model;

import java.io.Serializable;
import java.util.Date;

public class Account implements Serializable {
    /**
     * account.id (用户id)
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    private Integer id;

    /**
     * account.account (账号)
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    private String account;

    /**
     * account.password (密码)
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    private String password;

    /**
     * account.register_time (注册时间)
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    private Date registerTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }
}