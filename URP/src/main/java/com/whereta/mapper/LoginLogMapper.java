package com.whereta.mapper;

import com.whereta.model.LoginLog;
import org.springframework.stereotype.Component;

import java.util.List;
@Component("loginLogMapper")
public interface LoginLogMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(LoginLog record);

    int insertSelective(LoginLog record);

    LoginLog selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(LoginLog record);

    int updateByPrimaryKey(LoginLog record);

    List<LoginLog> query();
}