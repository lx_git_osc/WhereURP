package com.whereta.service;

import com.whereta.vo.ResultVO;

/**
 * Created by vincent on 15-9-24.
 */
public interface IMessageBoardService {

    ResultVO saveMessageBoard(int userId,String msg);
}
