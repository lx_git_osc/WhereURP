package com.whereta.dao.impl;

import com.whereta.dao.IMenuPermissionDao;
import com.whereta.mapper.MenuPermissionMapper;
import com.whereta.model.MenuPermission;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Set;

/**
 * Created by vincent on 15-8-27.
 */
@Repository("menuPermissionDao")
public class MenuPermissionDaoImpl implements IMenuPermissionDao {

    @Resource
    private MenuPermissionMapper menuPermissionMapper;

    /**
     * 根据菜单id获取权限id集合
     *
     * @param menuId
     * @return
     */
    public Set<Integer> selectPermissionIdSet(int menuId) {
        return menuPermissionMapper.selectPermissionIdSet(menuId);
    }

    /**
     * 根据权限id删除menu-per
     *
     * @param perId
     */
    public void deleteByPerId(int perId) {
        menuPermissionMapper.delete(null, perId);
    }

    /**
     * 根据菜单id删除menu-per
     *
     * @param menuId
     */
    public void deleteByMenuId(int menuId) {
        menuPermissionMapper.delete(menuId, null);
    }

    /**
     * 添加菜单授权
     * @param menuPermission
     * @return
     */
    public int addMenuPermission(MenuPermission menuPermission) {
        return menuPermissionMapper.insertSelective(menuPermission);
    }
}
